const User = require("./models/user");
const Note = require("./models/note");

const {ServerApp, ConsoleBrowser, InputForm} = require("webprogbase-console-view");

const app = new ServerApp();
const browser = new ConsoleBrowser();

app.use("/", function(req, res)
{
    const links = 
    {
        "users": "Users submenu",
        "notes": "Notes submenu",
    };

    res.send("Hello!!!", links);
});

app.use("users", function(req, res)
{
    const links = 
    {
        "getAllUsers": "Show all users",
        "getUser": "Slect user",
    };

    res.send("It's users submenu!!!", links);
});

app.use("getAllUsers", function(req, res)
{
    const users = User.getAll();
    let usersListText = "";

    for(let user of users)
    {
        usersListText += `\n-> ${user.id})   Login: ${user.login}
        Role: ${user.role}
        Fullname: ${user.fullname}
        Registered at: ${user.registeredAt}
        Avatar Url: ${user.avaUrl}
        Is disabled: ${user.isDisabled}\n`;
    }

    res.send(usersListText);
});

app.use("getUser", function(req, res)
{
    const fields = 
    {
        "id": "Enter user id",
    };

    const nextState = "showUser";  
    const form = new InputForm(nextState, fields);

    res.send("Select user!", form);
});

app.use("showUser", function(req, res)
{
    const userIdStr = req.data.id;
    const userId = parseInt(userIdStr); //@todo checks
    const user = User.getById(userId);

    if(!user)
    {
        res.redirect("getUser");
        return;
    }

    res.send(`You've selected user with id ${userId}:\n
        \rLogin: ${user.login}
        \rFullname: ${user.fullname}
        \rRegistered at: ${user.registeredAt}
        \rAvatar Url: ${user.avaUrl}
        \rIs disabled: ${user.isDisabled}\n`);
});


app.use("notes", function(req, res)
{
    const links = 
    {
        "insertNote": "Add note",
        "getAllNotes": "Show all notes",
        "getNote": "Slect note",
        "updateNote": "Update note",
        "deleteNote": "Delete note",
    };

    res.send("It's notes submenu!!!", links);
});

app.use("getAllNotes", function(req, res)
{
    const notes = Note.getAll();
    let notesListText = "";

    for(let note of notes)
    {
        notesListText += `\n-> ${note.id})   Note: ${note.note}
        User: ${note.user}
        Importance: ${note.importance}
        Length at: ${note.length}
        Date of creation Url: ${note.dateOfCreation}\n`;
    }

    res.send(notesListText);
});

app.use("getNote", function(req, res)
{
    const fields = 
    {
        "id": "Enter note id",
    };

    const nextState = "showNote";  
    const form = new InputForm(nextState, fields);

    res.send("Select note!", form);
});

app.use("showNote", function(req, res)
{
    const noteIdStr = req.data.id;
    const noteId = parseInt(noteIdStr); //@todo checks
    const note = Note.getById(noteId);

    if(!note)
    {
        res.redirect("getNote");
        return;
    }

    res.send(`You've selected note with id ${noteId}:\n
        \rNote: ${note.note}
        \rUser: ${note.user}
        \rImportance: ${note.importance}
        \rLength at: ${note.length}
        \rDate of creation Url: ${note.dateOfCreation}\n`);
});

app.use("deleteNote", function(req, res)
{
    const fields = 
    {
        "id": "Enter note id",
    };

    const nextState = "showDeletedNote";  
    const form = new InputForm(nextState, fields);

    res.send("Select note!", form);
});

app.use("showDeletedNote", function(req, res)
{
    const noteIdStr = req.data.id;
    const noteId = parseInt(noteIdStr); 
    const note = Note.delete(noteId);

    if(!note)
    {
        res.redirect("deleteNote");
        return;
    }

    res.send(`You've deleted note with id ${noteId}:\n
        \rNote: ${note.note}
        \rUser: ${note.user}
        \rImportance: ${note.importance}
        \rLength at: ${note.length}
        \rDate of creation Url: ${note.dateOfCreation}\n`);
});

app.use("insertNote", function(req, res)
{
    const fields = 
    {
        "note": "Enter your note",
        "user": "Enter your login",
        "importance": "Enter note importance",
    };

    const nextState = "showInsertedNote";  
    const form = new InputForm(nextState, fields);

    res.send("Select note!", form);
});

app.use("showInsertedNote", function(req, res)
{
    const noteText = req.data.note;
    const noteUser = req.data.user;
    const noteImportance = parseInt(req.data.importance);

    let today = new Date();
    today = today.getFullYear() + "-" + today.getMonth() + "-" + today.getDate() + "T" +
        today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds() + "Z";

    const note = Note.insert(noteText, noteUser, noteImportance, noteText.length, today);

    res.send(`You've inserted note with id ${note.id}:\n
        \rNote: ${note.note}
        \rUser: ${note.user}
        \rImportance: ${note.importance}
        \rLength at: ${note.length}
        \rDate of creation Url: ${note.dateOfCreation}\n`);
});

app.use("updateNote", function(req, res)
{
    const fields = 
    {
        "id": "Select note",
        "note": "Enter your note",
        "user": "Enter your login",
        "importance": "Enter note importance",
    };

    const nextState = "showUpdatedNote";  
    const form = new InputForm(nextState, fields);

    res.send("Select note!", form);
});

app.use("showUpdatedNote", function(req, res)
{
    const noteId = parseInt(req.data.id);
    const noteText = req.data.note;
    const noteUser = req.data.user;
    const noteImportance = parseInt(req.data.importance);

    let today = new Date();
    today = today.getFullYear() + "-" + today.getMonth() + "-" + today.getDate() + "T" +
        today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds() + "Z";

    const note = Note.update(noteId, noteText, noteUser, noteImportance, noteText.length, today);

    if(!note)
    {
        res.redirect("updateNote");
        return;
    }

    res.send(`You've updated note with id ${note.id}:\n
        \rNote: ${note.note}
        \rUser: ${note.user}
        \rImportance: ${note.importance}
        \rLength at: ${note.length}
        \rDate of creation Url: ${note.dateOfCreation}\n`);
});

const PORT = 4040;
app.listen(PORT);
browser.open(PORT);