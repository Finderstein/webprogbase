function User(id, login, role, fullname, registeredAt, avaUrl, isDisabled)
{
    this.id = id;
    this.login = login;
    this.role = role;
    this.fullname = fullname;
    this.registeredAt = registeredAt;
    this.avaUrl = avaUrl;
    this.isDisabled = isDisabled;
}

const fs = require('fs');

const json = JSON.parse(fs.readFileSync('data/users.json').toString());

let users = [];
for(let i = 0; i < json.items.length; i++)
{
    users.push(new User(json.items[i].id,
        json.items[i].login,
        json.items[i].role,
        json.items[i].fullname,
        json.items[i].registeredAt,
        json.items[i].avaUrl,
        json.items[i].isDisabled,));
}

module.exports = 
{
    getById: function(id)
    {
        return users.find(x => x.id === id);
    },
    getAll: function()
    {
        return users;
    }
};