function Note(id, note, user, importance, length, dateOfCreation)
{
    this.id = id;
    this.note = note;
    this.user = user;
    this.importance = importance;
    this.length = length;
    this.dateOfCreation = dateOfCreation;
}

const fs = require('fs');

const json = JSON.parse(fs.readFileSync('data/notes.json').toString());

let notes = [];
for(let i = 0; i < json.items.length; i++)
{
    notes.push(new  Note(json.items[i].id,
        json.items[i].note,
        json.items[i].user,
        json.items[i].importance,
        json.items[i].length,
        json.items[i].dateOfCreation));
}

module.exports = 
{
    getById: function(id)
    {
        return notes.find(x => x.id === id);
    },
    getAll: function()
    {
        return notes;
    },
    insert: function(noteText, user, importance, length, dateOfCreation)
    {
        const note = new Note(json.nextId, noteText, user, importance, length, dateOfCreation);
        json.nextId++;
        notes.push(note);

        json.items = JSON.parse(JSON.stringify(notes));


        try
        {
            fs.writeFileSync('data/notes.json', JSON.stringify(json, null, "\t"));
        }
        catch (error)
        {
            throw new Error("Can't save to file!");
        }
        

        return note;
    },
    update: function(id, noteText, user, importance, length, dateOfCreation)
    {
        const oldNote = notes.find(x => x.id === id);
        if(!oldNote)
        {
            return oldNote;
        }

        const index = notes.indexOf(oldNote);
        if (index > -1) {
            notes.splice(index, 1);
        }

        const note = new Note(id, noteText, user, importance, length, dateOfCreation);
        notes.push(note);
        
        json.items = JSON.parse(JSON.stringify(notes));

        try
        {
            fs.writeFileSync('data/notes.json', JSON.stringify(json, null, "\t"));
        }
        catch (error)
        {
            throw new Error("Can't save to file!");
        }

        return note;
    },
    delete: function(id)
    {
        const note = notes.find(x => x.id === id);
        if(!note)
        {
            return note;
        }

        const index = notes.indexOf(note);
        if (index > -1) {
            notes.splice(index, 1);
        }

        json.items = JSON.parse(JSON.stringify(notes));

        json.nextId--;

        try
        {
            fs.writeFileSync('data/notes.json', JSON.stringify(json, null, "\t"));
        }
        catch (error)
        {
            throw new Error("Can't save to file!");
        }

        return note;
    }
};


// {
//     "nextId": 2,
//     "items": [
//         {
//             "id": 0,
//             "note": "asdasd",
//             "user": "user",
//             "importance": 3,
//             "length": "6",
//             "dateOfCreation": "2018-09-03T09:03:11Z"
//         },
// 	    {
//             "id": 1,
//             "note": "aaaaaaa",
//             "user": "user1",
//             "importance": 1,
//             "length": 7,
//             "dateOfCreation": "2018-09-25T09:03:11Z"
//         }
//     ]
// }