const express = require('express');

const List = require("../models/list");

const busboyBodyParser = require('busboy-body-parser');

const router = express.Router();

router.use(busboyBodyParser());

const itemsOnPage = 5;
 
router.get('/', checkAuth, function(req, res)
{
    List.getAll(req.user._id)
        .then(allLists =>
        {
            let admin = false;
            if(req.user) 
                admin = req.user.role === "Admin";

            let lists = [];
            let page = [];

            let len = allLists.length;
            let paramPage = 1;
            if(req.query.page) paramPage = parseInt(req.query.page);
            if(!req.query.nTitle || req.query.nTitle === "")
            {
                let numOfPages = Math.ceil(len/itemsOnPage);

                if(numOfPages !== 1 && numOfPages !== 0)
                {
                    if(paramPage !== 1) 
                    {
                        let i = paramPage - 1;
                        page.push({numOfPage: i, strPage: "<<"});
                    }
                    for(let i = 1; i <= numOfPages; i++)
                    {
                        page.push({numOfPage: i, strPage: i});
                    }
                    if(paramPage !== numOfPages) 
                    {
                        let i = paramPage + 1;
                        page.push({numOfPage: i, strPage: ">>"});
                    }
                }


                for(let i = 0; itemsOnPage*(paramPage - 1) + i < len && i < itemsOnPage; i++)
                {
                    lists.push(allLists[itemsOnPage*(paramPage - 1) + i]);
                }

                res.render('lists', { lists, page, user: req.user, admin: admin });
            }
            else
            {
                let str = req.query.nTitle.toString();
                const result = "(" + str + ")";
                let searchNotes = [];
                for(let i = 0; i < len; i++)
                {
                    let title = allLists[i].title.toString();
                    
                    if(title.indexOf(str) !== -1)
                    {
                        searchNotes.push(allLists[i]);
                    }
                }

                let searchLen = searchNotes.length;
                let numOfPages = Math.ceil(searchLen/itemsOnPage);
                
                if(numOfPages !== 1 && numOfPages !== 0)
                {
                    if(paramPage !== 1) 
                    {
                        let p = (paramPage - 1) + '&nTitle=' + str;
                        page.push({numOfPage: p, strPage: "<<"});
                    }
                    for(let i = 1; i <= numOfPages; i++)
                    {
                        let p = i + '&nTitle=' + str;
                        page.push({numOfPage: p, strPage: i});
                    }
                    if(paramPage !== numOfPages) 
                    {
                        let p = (paramPage + 1) + '&nTitle=' + str;
                        page.push({numOfPage: p, strPage: ">>"});
                    }
                }
                
                for(let i = 0; itemsOnPage*(paramPage - 1) + i < searchLen && i < itemsOnPage; i++)
                {
                    lists.push(searchNotes[itemsOnPage*(paramPage - 1) + i]);
                }

                res.render('lists', { lists, search: result, page, user: req.user, admin: admin });
            }
        })
        .catch(err => res.status(500).send(err.toString()));
});

router.get('/new', checkAuth, function(req, res)
{
    let admin = false;
    if(req.user) 
        admin = req.user.role === "Admin";

    res.render("newlist", { user: req.user, admin: admin });
});

router.post('/new', checkAuth, function(req, res)
{
    List.insert(new List(req.body.nTitle, req.user._id, req.body.description, parseInt(req.body.importance), Date(), req.body.access))
        .then(list => res.redirect('/lists/' + list._id + '/notes'))
        .catch(err => res.status(500).send(err.toString()));
});


router.get('/:id', checkAuth, function(req, res)
{
    List.getById(req.params.id)
        .then(list => 
        {
            if(String(req.user._id) !== String(list.userId)) res.redirect('/');
            res.redirect('/lists/' + req.params.id + '/notes');
        })
        .catch(err => res.status(500).send(err.toString()));
});

function checkAuth(req, res, next) {
    if (!req.user) return res.redirect('/'); // 'Not authorized'
    next();  // пропускати далі тільки аутентифікованих
}
 
module.exports = router;