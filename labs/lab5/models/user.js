const mongoose = require('mongoose');

const UserShema = new mongoose.Schema({
    login: { type: String, required: true },
    fullname: { type: String, required: true },
    role: { type: Number, required: true },
    bio: { type: String },
    registeredAt: { type: String, default: new Date().toISOString() },
    avaUrl: { type: String, default: "/images/users/no_ava.png" },
    isDisabled: { type: Boolean, required: true },
});

const UserModel = mongoose.model('User', UserShema);

class User
{
    constructor(login, role, fullname, bio, registeredAt, avaUrl, isDisabled)
    {
        this.login = login;
        this.role = role;
        this.fullname = fullname;
        this.bio = bio;
        this.registeredAt = registeredAt;
        this.avaUrl = avaUrl;
        this.isDisabled = isDisabled;
    }

    static getById(id)
    {
        return UserModel.findById({ _id: id});
    }
    
    static getAll()
    {
        return UserModel.find();
    }

    static insert(user)
    {
        return new UserModel(user).save();
    }

    static update(id, user)
    {
        return UserModel.findByIdAndUpdate(id, user);
    }

    static delete(id)
    {
        return UserModel.findByIdAndDelete(id);
    }
}

module.exports = User;