const express = require('express');
const mustache = require('mustache-express');
const path = require('path');

const Note = require("./models/note");
const User = require("./models/user");
const List = require("./models/list");

const bodyParser = require('body-parser');
const busboyBodyParser = require('busboy-body-parser');

const mongoose = require('mongoose');

const app = express();

const usersRouter = require('./routes/users');
const notesRouter = require('./routes/notes');
const listsRouter = require('./routes/lists');
app.use('/lists', listsRouter);
app.use('/', notesRouter);
app.use('/users', usersRouter);

const viewsDir = path.join(__dirname, 'views');
app.engine("mst", mustache(path.join(viewsDir, "partials")));
app.set('views', viewsDir);
app.set('view engine', 'mst');

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
app.use(busboyBodyParser({ limit: '5mb' }));

app.use(express.static('public'));

const PORT = 3033;
const databaseUrl = 'mongodb://localhost:27017/lab5';
const connectOptions = { useNewUrlParser: true};

mongoose.connect(databaseUrl, connectOptions)
    .then(() => console.log(`Database connected: ${databaseUrl}`))
    .then(() => app.listen(PORT, function() { console.log('Server is ready'); }))
    .catch(err => console.log(`Start error ${err}`));

app.get('/addList', function(req, res)
{
    let today = new Date();
    today = today.getFullYear() + "-" + today.getMonth() + "-" + today.getDate() + "T" +
        today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds() + "Z";

    List.insert(new List('TestList', 'test', 'test', null, 3, today.toString(), 1))
    .then(() => res.redirect('/lists'))
    .catch(err => res.status(500).send(err.toString()));
});
    
// usage
app.get('/', function(req, res)
{
    res.render('index', {});
});

app.get('/about', function(req, res)
{
    res.render('about', {});
});


app.get('/api/users', function(req, res)
{
    User.getAll()
        .then(users => res.end(JSON.stringify(users, null, "\t")))
        .catch(err => res.status(500).send(err.toString()));
});

app.get('/api/users/:id', function(req, res)
{
    User.getById(req.params.id)
        .then(user => res.end(JSON.stringify(user, null, "\t")))
        .catch(err => res.status(500).send(err.toString()));
});


app.get('/api/notes', function(req, res)
{
    Note.getAll()
        .then(notes => res.end(JSON.stringify(notes, null, "\t")))
        .catch(err => res.status(500).send(err.toString()));
});



app.get('/api/notes/:id', function(req, res)
{
    Note.getById(req.params.id)
        .then(note => res.end(JSON.stringify(note, null, "\t")))
        .catch(err => res.status(500).send(err.toString()));
});