const express = require('express');

const fs = require('fs.promised');

const Note = require("../models/note");
const List = require("../models/list");

const busboyBodyParser = require('busboy-body-parser');

const router = express.Router();

router.use(busboyBodyParser());

const itemsOnPage = 5;

const path = require('path');

// router.post('/:id', function(req, res)
// {
//     List.delete(req.params.id)
//         .then(() => res.redirect('/lists'))
//         .catch(err => res.status(500).send(err.toString()));

// });
 
router.get('/lists/:listId/notes', function(req, res)
{
    List.getById(req.params.listId)
        .then(list => {return Promise.all([list, Note.getAll(list._id)]);})
        .then(([list, allNotes]) =>
            {
                let notes = [];
                let page = [];
    
                let len = allNotes.length;
                let paramPage = 1;
                if(req.query.page) paramPage = parseInt(req.query.page);
                if(!req.query.nTitle || req.query.nTitle === "")
                {
                    let numOfPages = Math.ceil(len/itemsOnPage);
    
                    if(numOfPages !== 1 && numOfPages !== 0)
                    {
                        if(paramPage !== 1) 
                        {
                            let i = paramPage - 1;
                            page.push({numOfPage: i, strPage: "<<"});
                        }
                        for(let i = 1; i <= numOfPages; i++)
                        {
                            page.push({numOfPage: i, strPage: i});
                        }
                        if(paramPage !== numOfPages) 
                        {
                            let i = paramPage + 1;
                            page.push({numOfPage: i, strPage: ">>"});
                        }
                    }
    
    
                    for(let i = 0; itemsOnPage*(paramPage - 1) + i < len && i < itemsOnPage; i++)
                    {
                        notes.push(allNotes[itemsOnPage*(paramPage - 1) + i]);
                    }
    
                    res.render('notes', {notes, page, list});
                }
                else
                {
                    let str = req.query.nTitle.toString();
                    const result = "(" + str + ")";
                    let searchNotes = [];
                    for(let i = 0; i < len; i++)
                    {
                        let title = allNotes[i].title.toString();
                        
                        if(title.indexOf(str) !== -1)
                        {
                            searchNotes.push(allNotes[i]);
                        }
                    }
    
                    let searchLen = searchNotes.length;
                    let numOfPages = Math.ceil(searchLen/itemsOnPage);
                    
                    if(numOfPages !== 1 && numOfPages !== 0)
                    {
                        if(paramPage !== 1) 
                        {
                            let p = (paramPage - 1) + '&nTitle=' + str;
                            page.push({numOfPage: p, strPage: "<<"});
                        }
                        for(let i = 1; i <= numOfPages; i++)
                        {
                            let p = i + '&nTitle=' + str;
                            page.push({numOfPage: p, strPage: i});
                        }
                        if(paramPage !== numOfPages) 
                        {
                            let p = (paramPage + 1) + '&nTitle=' + str;
                            page.push({numOfPage: p, strPage: ">>"});
                        }
                    }
                    
                    for(let i = 0; itemsOnPage*(paramPage - 1) + i < searchLen && i < itemsOnPage; i++)
                    {
                        notes.push(searchNotes[itemsOnPage*(paramPage - 1) + i]);
                    }
    
                    res.render('notes', {notes, search: result, page, list});
                }
            })
        .catch(err => res.status(500).send(err.toString()));
});

router.get('/lists/:listId/notes/new', function(req, res)
{
    res.render("newnote", {listId: req.params.listId});
});

router.post('/lists/:listId/notes/delete', function(req, res)
{
    List.delete(req.params.listId)
        .then(() => res.redirect('/lists'))
        .catch(err => res.status(500).send(err.toString()));
});

router.get('/lists/:listId/notes/update', function(req, res)
{
    List.getById(req.params.listId)
        .then(list => res.render("updatelist", {list}))
        .catch(err => res.status(500).send(err.toString()));
});

router.post('/lists/:listId/notes/update', function(req, res)
{
    let today = new Date();
    today = today.getFullYear() + "-" + today.getMonth() + "-" + today.getDate() + "T" +
        today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds() + "Z";

    List.update(req.params.listId, req.body.nTitle, req.body.description, parseInt(req.body.importance), today.toString(), req.body.access)
        .then(list => res.redirect('/lists/' + list._id + '/notes'))
        .catch(err => res.status(500).send(err.toString()));
});

router.post('/lists/:listId/notes/new', function(req, res)
{
    const file = req.files.file;

    let today = new Date();
    today = today.getFullYear() + "-" + today.getMonth() + "-" + today.getDate() + "T" +
        today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds() + "Z";

    let fileName = "";
    if(file)
    {
        fileName = today.toString() + '_' + req.body.login + file.name.substr(file.name.lastIndexOf("."));
        let filePath = path.join(__dirname, '../') + "/data/fs/" + fileName;
        fs.writeFile(filePath, file.data)
        .catch(err => {throw err;});
    }

    Note.insert(new Note(req.params.listId, req.body.nTitle, req.body.note, req.body.login, parseInt(req.body.importance), req.body.note.length, fileName, today.toString()))
        .then(note => res.redirect('/lists/'+ req.params.listId + '/notes/' + note._id))
        .catch(err => res.status(500).send(err.toString()));
});

router.get('/lists/:listId/notes/:noteId/update', function(req, res)
{
    Note.getById(req.params.noteId)
        .then(note => res.render("updatenote", { note, listId: req.params.noteId }))
        .catch(err => res.status(500).send(err.toString()));
});

router.post('/lists/:listId/notes/:noteId/update', function(req, res)
{
    let today = new Date();
    today = today.getFullYear() + "-" + today.getMonth() + "-" + today.getDate() + "T" +
        today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds() + "Z";

    Note.update(req.params.noteId, req.body.nTitle, req.body.note, parseInt(req.body.importance), req.body.note.length, today.toString())
        .then(note => res.redirect('/lists/' + req.params.listId + '/notes/' + note._id))
        .catch(err => res.status(500).send(err.toString()));
});


router.get('/lists/:listId/notes/:noteId', function(req, res)
{
    Note.getById(req.params.noteId)
        .then(note => res.render('note', {note: note, listId: req.params.listId}))
        .catch(err => res.status(500).send(err.toString()));
});

router.get('/lists/:listId/notes/:noteId/download/:file', function(req, res)
{
    res.sendFile(path.join(__dirname, '../') + "/data/fs/"+ req.params.file);
});

router.get('/lists/:listId/notes/:noteId/download/', function(req, res)
{
    res.redirect('/lists/'+ req.params.listId + '/notes/' + req.params.noteId);
});

router.post('/lists/:listId/notes/:noteId/delete', function(req, res)
{
    Note.delete(req.params.noteId)
        .then(() => res.redirect('/lists/'+ req.params.listId + '/notes'))
        .catch(err => res.status(500).send(err.toString()));

});
 
module.exports = router;