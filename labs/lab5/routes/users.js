const express = require('express');

const User = require("../models/user");

const router = express.Router();


// app.get('/addUser', function(req, res)
// {
//     User.insert(new User("admin", 0, "Admin", "I was born in French. I am 19 years old and i am a girl. I love badminton and Dota 2. Moreover, I like to go for a walk with my friends.", "2018-09-03T09:03:11Z", "/images/users/admin.png", false))
//         .then(() => res.redirect('/users'))
//         .catch(err => res.status(500).send(err.toString()));
// });

router.get('/', function(req, res)
{
    User.getAll()
        .then(users => res.render('users', {users}))
        .catch(err => res.status(500).send(err.toString()));
});


router.get('/:id', function(req, res)
{
    User.getById(req.params.id)
        .then(user => res.render('user', user))
        .catch(err => res.status(500).send(err.toString()));
});

module.exports = router;