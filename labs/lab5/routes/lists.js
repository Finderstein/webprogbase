const express = require('express');

const List = require("../models/list");

const busboyBodyParser = require('busboy-body-parser');

const router = express.Router();

router.use(busboyBodyParser());

const itemsOnPage = 5;
 
router.get('/', function(req, res)
{
    List.getAll()
        .then(allLists =>
        {
            let lists = [];
            let page = [];

            let len = allLists.length;
            let paramPage = 1;
            if(req.query.page) paramPage = parseInt(req.query.page);
            if(!req.query.nTitle || req.query.nTitle === "")
            {
                let numOfPages = Math.ceil(len/itemsOnPage);

                if(numOfPages !== 1 && numOfPages !== 0)
                {
                    if(paramPage !== 1) 
                    {
                        let i = paramPage - 1;
                        page.push({numOfPage: i, strPage: "<<"});
                    }
                    for(let i = 1; i <= numOfPages; i++)
                    {
                        page.push({numOfPage: i, strPage: i});
                    }
                    if(paramPage !== numOfPages) 
                    {
                        let i = paramPage + 1;
                        page.push({numOfPage: i, strPage: ">>"});
                    }
                }


                for(let i = 0; itemsOnPage*(paramPage - 1) + i < len && i < itemsOnPage; i++)
                {
                    lists.push(allLists[itemsOnPage*(paramPage - 1) + i]);
                }

                res.render('lists', {lists, page});
            }
            else
            {
                let str = req.query.nTitle.toString();
                const result = "(" + str + ")";
                let searchNotes = [];
                for(let i = 0; i < len; i++)
                {
                    let title = allLists[i].title.toString();
                    
                    if(title.indexOf(str) !== -1)
                    {
                        searchNotes.push(allLists[i]);
                    }
                }

                let searchLen = searchNotes.length;
                let numOfPages = Math.ceil(searchLen/itemsOnPage);
                
                if(numOfPages !== 1 && numOfPages !== 0)
                {
                    if(paramPage !== 1) 
                    {
                        let p = (paramPage - 1) + '&nTitle=' + str;
                        page.push({numOfPage: p, strPage: "<<"});
                    }
                    for(let i = 1; i <= numOfPages; i++)
                    {
                        let p = i + '&nTitle=' + str;
                        page.push({numOfPage: p, strPage: i});
                    }
                    if(paramPage !== numOfPages) 
                    {
                        let p = (paramPage + 1) + '&nTitle=' + str;
                        page.push({numOfPage: p, strPage: ">>"});
                    }
                }
                
                for(let i = 0; itemsOnPage*(paramPage - 1) + i < searchLen && i < itemsOnPage; i++)
                {
                    lists.push(searchNotes[itemsOnPage*(paramPage - 1) + i]);
                }

                res.render('lists', {lists, search: result, page});
            }
        })
        .catch(err => res.status(500).send(err.toString()));
});

router.get('/new', function(req, res)
{
    res.render("newlist", {});
});

router.post('/new', function(req, res)
{
    let today = new Date();
    today = today.getFullYear() + "-" + today.getMonth() + "-" + today.getDate() + "T" +
        today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds() + "Z";

    List.insert(new List(req.body.nTitle, req.body.user, req.body.description, parseInt(req.body.importance), today.toString(), req.body.access))
        .then(list => res.redirect('/lists/' + list._id + '/notes'))
        .catch(err => res.status(500).send(err.toString()));
});


router.get('/:id', function(req, res)
{
    List.getById(req.params.id)
        .then(() => res.redirect('/lists/' + req.params.id + '/notes'))
        .catch(err => res.status(500).send(err.toString()));
});
 
module.exports = router;