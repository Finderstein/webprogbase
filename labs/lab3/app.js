const express = require('express');
const mustache = require('mustache-express');
const path = require('path');
const fs = require('fs');

const User = require("./models/user");
const Note = require("./models/note");

const app = express();

const viewsDir = path.join(__dirname, 'views');
app.engine("mst", mustache(path.join(viewsDir, "partials")));
app.set('views', viewsDir);
app.set('view engine', 'mst');

const PORT = 3030;
app.use(express.static('public'));
app.listen(PORT, function() { console.log('Server is ready'); });
 

// usage
app.get('/', function(req, res) {
    res.render('index', {});
});

app.get('/about', function(req, res) {
    res.render('about', {});
});

app.get('/users', function(req, res) {
    const users = User.getAll();
    const data = {users};
    res.render('users', data);
});

app.get('/notes', function(req, res) {
    const notes = Note.getAll();
    const data = {notes};
    res.render('notes', data);
});

app.get('/users/:id', function(req, res) {
    const users = User.getAll();
    if(users.find(x => x.id === parseInt(req.params.id)))
        res.render('user', User.getById(parseInt(req.params.id)));
    else
    {
        res.writeHead(404, {'Content-Type': 'text/plain'});
        res.end("Can't find such user");
    }
});

app.get('/notes/:id', function(req, res) {
    const notes = Note.getAll();
    if(notes.find(x => x.id === parseInt(req.params.id)))
        res.render('note', Note.getById(parseInt(req.params.id)));
    else
    {
        res.writeHead(404, {'Content-Type': 'text/plain'});
        res.end("Can't find such note");
    }
});

function readWebDoc(fileName) {
    const filePath = path.join(__dirname, fileName);
    return fs.readFileSync(filePath);
};
 

app.get('/api/users', function(req, res)
{
    res.writeHead(200, {'Content-Type': 'application/json'});
    const users = User.getAll();
    res.end(JSON.stringify(users));
});

app.get('/api/notes', function(req, res)
{
    res.writeHead(200, {'Content-Type': 'application/json'});
    const notes = Note.getAll();
    res.end(JSON.stringify(notes));
});

app.get('/api/users/:id', function(req, res)
{
    const users = User.getAll();
    if(users.find(x => x.id === parseInt(req.params.id)))
    {
        res.writeHead(200, {'Content-Type': 'application/json'});
        res.end(JSON.stringify(users.find(x => x.id === parseInt(req.params.id))));
    }
    else
    {
        res.writeHead(404, {'Content-Type': 'text/plain'});
        res.end("Can't find such user");
    }
});

app.get('/api/notes/:id', function(req, res)
{
    const notes = Note.getAll();
    if(notes.find(x => x.id === parseInt(req.params.id)))
    {
        res.writeHead(200, {'Content-Type': 'application/json'});
        res.end(JSON.stringify(notes.find(x => x.id === parseInt(req.params.id))));
    }
    else
    {
        res.writeHead(404, {'Content-Type': 'text/plain'});
        res.end("Can't find such note");
    }
});