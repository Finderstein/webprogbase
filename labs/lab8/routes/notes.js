const express = require('express');

const Note = require("../models/note");
const List = require("../models/list");

const busboyBodyParser = require('busboy-body-parser');

const config = require('../config');

const cloudinary = require('cloudinary');
cloudinary.config({
    cloud_name: config.cloudinary.cloud_name,
    api_key: config.cloudinary.api_key,
    api_secret: config.cloudinary.api_secret
});

const router = express.Router();

router.use(busboyBodyParser());

const itemsOnPage = 5;
 
router.get('/lists/:listId/notes', checkAuth, function(req, res)
{
    List.getById(req.params.listId)
        .then(list => {return Promise.all([list, Note.getAll(list._id)]);})
        .then(([list, allNotes]) =>
            {
                let admin = false;
                if(req.user) 
                    admin = req.user.role === "Admin";

                let notes = [];
                let page = [];
    
                let len = allNotes.length;
                let paramPage = 1;
                if(req.query.page) paramPage = parseInt(req.query.page);
                if(!req.query.nTitle || req.query.nTitle === "")
                {
                    let numOfPages = Math.ceil(len/itemsOnPage);
    
                    if(numOfPages !== 1 && numOfPages !== 0)
                    {
                        if(paramPage !== 1) 
                        {
                            let i = paramPage - 1;
                            page.push({numOfPage: i, strPage: "<<"});
                        }
                        for(let i = 1; i <= numOfPages; i++)
                        {
                            page.push({numOfPage: i, strPage: i});
                        }
                        if(paramPage !== numOfPages) 
                        {
                            let i = paramPage + 1;
                            page.push({numOfPage: i, strPage: ">>"});
                        }
                    }
    
    
                    for(let i = 0; itemsOnPage*(paramPage - 1) + i < len && i < itemsOnPage; i++)
                    {
                        notes.push(allNotes[itemsOnPage*(paramPage - 1) + i]);
                    }
    
                    res.render('notes', { notes, page, list, user: req.user, admin: admin });
                }
                else
                {
                    let str = req.query.nTitle.toString();
                    const result = "(" + str + ")";
                    let searchNotes = [];
                    for(let i = 0; i < len; i++)
                    {
                        let title = allNotes[i].title.toString();
                        
                        if(title.indexOf(str) !== -1)
                        {
                            searchNotes.push(allNotes[i]);
                        }
                    }
    
                    let searchLen = searchNotes.length;
                    let numOfPages = Math.ceil(searchLen/itemsOnPage);
                    
                    if(numOfPages !== 1 && numOfPages !== 0)
                    {
                        if(paramPage !== 1) 
                        {
                            let p = (paramPage - 1) + '&nTitle=' + str;
                            page.push({numOfPage: p, strPage: "<<"});
                        }
                        for(let i = 1; i <= numOfPages; i++)
                        {
                            let p = i + '&nTitle=' + str;
                            page.push({numOfPage: p, strPage: i});
                        }
                        if(paramPage !== numOfPages) 
                        {
                            let p = (paramPage + 1) + '&nTitle=' + str;
                            page.push({numOfPage: p, strPage: ">>"});
                        }
                    }
                    
                    for(let i = 0; itemsOnPage*(paramPage - 1) + i < searchLen && i < itemsOnPage; i++)
                    {
                        notes.push(searchNotes[itemsOnPage*(paramPage - 1) + i]);
                    }
    
                    res.render('notes', { notes, search: result, page, list, user: req.user, admin: admin });
                }
            })
        .catch(err => res.status(500).send(err.toString()));
});

router.get('/lists/:listId/notes/new', checkAuth, function(req, res)
{
    let admin = false;
    if(req.user) 
        admin = req.user.role === "Admin";

    res.render("newnote", { listId: req.params.listId, user: req.user, admin: admin });
});

router.post('/lists/:listId/notes/delete', checkAuth, function(req, res)
{
    List.delete(req.params.listId)
        .then(() => res.redirect('/lists'))
        .catch(err => res.status(500).send(err.toString()));
});

router.get('/lists/:listId/notes/update', checkAuth, function(req, res)
{
    let admin = false;
    if(req.user) 
        admin = req.user.role === "Admin";

    List.getById(req.params.listId)
        .then(list => 
        {
            let publicAc = list.access === "public";
            res.render("updatelist", { list, user: req.user, admin: admin, public: publicAc});
        })
        .catch(err => res.status(500).send(err.toString()));
});

router.post('/lists/:listId/notes/update', checkAuth, function(req, res)
{
    List.update(req.params.listId, req.body.nTitle, req.body.description, parseInt(req.body.importance), Date(), req.body.access)
        .then(list => res.redirect('/lists/' + list._id + '/notes'))
        .catch(err => res.status(500).send(err.toString()));
});

router.post('/lists/:listId/notes/new', checkAuth, function(req, res)
{
    const file = req.files.file;

    if(file)
        cloudinary.v2.uploader.upload_stream({ resource_type: 'auto' }, function(error, result)
        { 
            Note.insert(new Note(req.params.listId, req.body.nTitle, req.body.note, req.user._id, parseInt(req.body.importance), req.body.note.length, result.url, file.name, Date()))
                .then(note => res.redirect('/lists/'+ req.params.listId + '/notes/' + note._id))
                .catch(err => res.status(500).send(err.toString()));
        })
        .end(file.data);
    else
        Note.insert(new Note(req.params.listId, req.body.nTitle, req.body.note, req.user._id, parseInt(req.body.importance), req.body.note.length, "", "No file", Date()))
            .then(note => res.redirect('/lists/'+ req.params.listId + '/notes/' + note._id))
            .catch(err => res.status(500).send(err.toString()));
});

router.get('/lists/:listId/notes/:noteId/update', checkAuth, function(req, res)
{
    let admin = false;
    if(req.user) 
        admin = req.user.role === "Admin";

    Note.getById(req.params.noteId)
        .then(note => res.render("updatenote", { note, listId: req.params.noteId, user: req.user, admin: admin }))
        .catch(err => res.status(500).send(err.toString()));
});

router.post('/lists/:listId/notes/:noteId/update', checkAuth, function(req, res)
{
    Note.update(req.params.noteId, req.body.nTitle, req.body.note, parseInt(req.body.importance), req.body.note.length, Date())
        .then(note => res.redirect('/lists/' + req.params.listId + '/notes/' + note._id))
        .catch(err => res.status(500).send(err.toString()));
});


router.get('/lists/:listId/notes/:noteId', checkAuth, function(req, res)
{
    let admin = false;
    if(req.user) 
        admin = req.user.role === "Admin";

    Note.getById(req.params.noteId)
        .then(note => res.render('note', { note: note, listId: req.params.listId, user: req.user, admin: admin }))
        .catch(err => res.status(500).send(err.toString()));
});

router.post('/lists/:listId/notes/:noteId/delete', checkAuth, function(req, res)
{
    Note.delete(req.params.noteId)
        .then(() => res.redirect('/lists/'+ req.params.listId + '/notes'))
        .catch(err => res.status(500).send(err.toString()));
});

function checkAuth(req, res, next) {
    if (!req.user) return res.redirect('/'); // 'Not authorized'
    next();  // пропускати далі тільки аутентифікованих
}
 
module.exports = router;