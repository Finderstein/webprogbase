const express = require('express');

const User = require("../models/user");

const router = express.Router();

router.get('/addUser', function(req, res)
{
    User.insert(new User("admin", "admin", "Admin", "Admin", "I was born in French. I am 19 years old and i am a girl. I love badminton and Dota 2. Moreover, I like to go for a walk with my friends.", Date(), "/images/users/admin.png", false))
        .then(() =>
        {
             return User.insert(new User("User1", "user1", "User", "UsEr1", "I was born in Canada. I am 25 years old. I love computer games and tennis. Also I like to go to the gym.", Date(), "/images/users/user1.jpg", false));
        })
        .then(() =>
        {
            return User.insert(new User("test", "test", "User", "Test", "TEST!!!", Date(), "/images/users/no_ava.png", false));
        })
        .then(() => res.redirect('/users'))
        .catch(err => res.status(500).send(err.toString()));
});


router.get('/', checkAdmin, function(req, res)
{
    let admin = false;
    if(req.user) 
        admin = req.user.role === "Admin";

    User.getAll()
        .then(users => res.render('users', { users, user: req.user, admin: admin }))
        .catch(err => res.status(500).send(err.toString()));
});

router.get('/:id', checkAuth, function(req, res)
{
    if(req.user.role !== "Admin" && String(req.user._id) !== String(req.params.id)) return res.redirect('/');
    let admin = false;
    if(req.user) 
        admin = req.user.role === "Admin";

    User.getById(req.params.id)
        .then(userInfo =>
        {
            let userRole = userInfo.role === "Admin";
            res.render('user', { userInfo, user: req.user, admin: admin, userRole: userRole });
        })
        .catch(err => res.status(500).send(err.toString()));
});

router.post('/:id', checkAdmin, function(req, res)
{
    User.getById(req.params.id)
        .then(userInfo =>
        {
            userInfo.role = req.body.role;
            return User.update(userInfo._id, userInfo);
        })
        .then(userInfo =>
        {
            if(req.user.login === userInfo.login)
                res.redirect('/about');
            else res.redirect('/users/' + req.params.id);
        })
        .catch(err => res.status(500).send(err.toString()));
});

function checkAuth(req, res, next) {
    if (!req.user) return res.redirect('/'); // 'Not authorized'
    next();  // пропускати далі тільки аутентифікованих
}

function checkAdmin(req, res, next) {
    if (!req.user) res.redirect('/'); // 'Not authorized'
    else if (req.user.role !== 'Admin') res.redirect('/'); // 'Forbidden'
    else next();  // пропускати далі тільки аутентифікованих із роллю 'admin'
}

module.exports = router;