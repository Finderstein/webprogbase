const express = require('express');
const mustache = require('mustache-express');
const path = require('path');

const User = require("./models/user");
const Note = require("./models/note");
const bodyParser = require('body-parser');
const busboyBodyParser = require('busboy-body-parser');

const app = express();

const viewsDir = path.join(__dirname, 'views');
app.engine("mst", mustache(path.join(viewsDir, "partials")));
app.set('views', viewsDir);
app.set('view engine', 'mst');

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
app.use(busboyBodyParser({ limit: '5mb' }));

const PORT = 3030;
app.use(express.static('public'));
app.listen(PORT, function() { console.log('Server is ready'); });


const itemsOnPage = 5;
 

// usage
app.get('/', function(req, res) {
    res.render('index', {});
});

app.get('/about', function(req, res) {
    res.render('about', {});
});

app.get('/users', function(req, res) {
    User.getAll(function(err, users)
    {
        if(err)
        {   
            res.writeHead(404, {'Content-Type': 'text/plain'});
            res .send(err.toString());
        }   
        else
        {
            const data = {users};
            res.render('users', data);
        }
    });
});

app.get('/notes', function(req, res) {
    Note.getAll(function(err, allNotes)
    {
        if(err)
        {   
            res.writeHead(404, {'Content-Type': 'text/plain'});
            res .send(err.toString());
        }   
        else
        {
            let notes = [];
            let page = [];

            let len = allNotes.length;
            let paramPage = 1;
            if(req.query.page) paramPage = parseInt(req.query.page);
            if(!req.query.nTitle || req.query.nTitle === "")
            {
                let numOfPages = Math.ceil(len/itemsOnPage);

                if(numOfPages !== 1 && numOfPages !== 0)
                {
                    if(paramPage !== 1) 
                    {
                        let i = paramPage - 1;
                        page.push({numOfPage: i, strPage: "<<"});
                    }
                    for(let i = 1; i <= numOfPages; i++)
                    {
                        page.push({numOfPage: i, strPage: i});
                    }
                    if(paramPage !== numOfPages) 
                    {
                        let i = paramPage + 1;
                        page.push({numOfPage: i, strPage: ">>"});
                    }
                }


                for(let i = 0; itemsOnPage*(paramPage - 1) + i < len && i < itemsOnPage; i++)
                {
                    notes.push(allNotes[itemsOnPage*(paramPage - 1) + i]);
                }

                res.render('notes', {notes, page});
            }
            else
            {
                let str = req.query.nTitle.toString();
                const result = "(" + str + ")";
                let searchNotes = [];
                for(let i = 0; i < len; i++)
                {
                    let title = allNotes[i].title.toString();
                    
                    if(title.indexOf(str) !== -1)
                    {
                        searchNotes.push(allNotes[i]);
                    }
                }

                let searchLen = searchNotes.length;
                let numOfPages = Math.ceil(searchLen/itemsOnPage);
                
                if(numOfPages !== 1 && numOfPages !== 0)
                {
                    if(paramPage !== 1) 
                    {
                        let p = (paramPage - 1) + '&nTitle=' + str;
                        page.push({numOfPage: p, strPage: "<<"});
                    }
                    for(let i = 1; i <= numOfPages; i++)
                    {
                        let p = i + '&nTitle=' + str;
                        page.push({numOfPage: p, strPage: i});
                    }
                    if(paramPage !== numOfPages) 
                    {
                        let p = (paramPage + 1) + '&nTitle=' + str;
                        page.push({numOfPage: p, strPage: ">>"});
                    }
                }
                
                for(let i = 0; itemsOnPage*(paramPage - 1) + i < searchLen && i < itemsOnPage; i++)
                {
                    notes.push(searchNotes[itemsOnPage*(paramPage - 1) + i]);
                }

                res.render('notes', {notes, search: result, page});
            }
        }
    });
});

app.get('/notes/new', function(req, res)
{
    res.render("newnote", {});
});

app.post('/notes/new', function(req, res)
{
    Note.insert(req.body.nTitle, req.body.note, req.body.login, parseInt(req.body.importance), req.files.file, function(err, note)
    {
        if(err)
        {   
            res .send(err.toString());
        }   
        else
        {
            const url = '/notes/' + note.id.toString();
            res.redirect(url);
        }
    });
});

app.get('/users/:id', function(req, res) {
    User.getById(parseInt(req.params.id), function(err, user)
    {
        if(err) 
        {   
            res.writeHead(404, {'Content-Type': 'text/plain'});
            res.send(err.toString());
        }   
        else
        {
            res.render('user', user);
        }
    });
});

app.get('/notes/:id', function(req, res) {
    Note.getById(parseInt(req.params.id), function(err, note)
    {
        if(err) 
        {   
            res.writeHead(404, {'Content-Type': 'text/plain'});
            res .send(err.toString());
        }   
        else
        {
            res.render('note', note);
        }
    });
});

app.get('/notes/:id/download/:file', function(req, res) {
    res.sendFile(__dirname + "/data/fs/"+ req.params.file);
});

app.get('/notes/:id/download/', function(req, res) {
    res.redirect("/notes/" + req.params.id);
});

app.post('/notes/:id', function(req, res)
{
    Note.delete(req.params.id, function(err)
    {
        if(err)
        {   
            res.send(err.toString());
        }   
        else
        {
            res.redirect('/notes');
        }
    });
});
 

app.get('/api/users', function(req, res)
{
    User.getAll(function(err, users)
    {
        res.writeHead(404, {'Content-Type': 'text/plain'});
        if(err)
        {   
            res.send(err.toString());
        }   
        else
        {
            res.end(JSON.stringify(users, null, "\t"));
        }
    });
});

app.get('/api/notes', function(req, res)
{
    Note.getAll(function(err, notes)
    {
        res.writeHead(404, {'Content-Type': 'text/plain'});
        if(err)
        {   
            res.send(err.toString());
        }   
        else
        {
            res.end(JSON.stringify(notes, null, "\t"));
        }
    });
});

app.get('/api/users/:id', function(req, res)
{
    User.getById(parseInt(req.params.id), function(err, user)
    {
        res.writeHead(404, {'Content-Type': 'text/plain'});
        if(err)
        {   
            res.send(err.toString());
        }   
        else
        {
            res.end(JSON.stringify(user, null, "\t"));
        }
    });
});

app.get('/api/notes/:id', function(req, res)
{
    Note.getById(parseInt(req.params.id), function(err, note)
    {
        res.writeHead(404, {'Content-Type': 'text/plain'});
        if(err)
        {   
            res.send(err.toString());
        }   
        else
        {
            res.end(JSON.stringify(note, null, "\t"));
        }
    });
});