class User
{
    constructor(id, login, role, fullname, bio, registeredAt, avaUrl, isDisabled)
    {
        this.id = id;
        this.login = login;
        this.role = role;
        this.fullname = fullname;
        this.bio = bio;
        this.registeredAt = registeredAt;
        this.avaUrl = avaUrl;
        this.isDisabled = isDisabled;
    }

    static getById(id, callback)
    {
        readJsonFile(function(err, users)
        {
            if(err) callback(err);
            else callback(null, users.find(x => x.id === id));
        });
    }
    
    static getAll(callback)
    {
        readJsonFile(function(err, users)
        {
            if(err) callback(err);
            else callback(null, users);
        });
    }
}

const fs = require('fs');

module.exports = User;

function readJsonFile(callback)
{
    fs.readFile('data/users.json', (err, data) =>
    {
        if(err)
            callback(new Error(err));
        else
        {
            const json = JSON.parse(data.toString());
            
            let users = [];
            for(let i = 0; i < json.items.length; i++)
            {
                users.push(new User(json.items[i].id,
                    json.items[i].login,
                    json.items[i].role,
                    json.items[i].fullname,
                    json.items[i].bio,
                    json.items[i].registeredAt,
                    json.items[i].avaUrl,
                    json.items[i].isDisabled,));
            }

            callback(null, users);
        }
    });
}





