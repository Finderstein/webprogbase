class Note
{
    constructor(id, title, note, user, importance, length, file, dateOfCreation)
    {
        this.id = id;
        this.title = title;
        this.note = note;
        this.user = user;
        this.importance = importance;
        this.length = length;
        this.file = file;
        this.dateOfCreation = dateOfCreation;
    }

    static getById(id, callback)
    {
        readJsonFile(function(err, json)
        {
            if(err) callback(err);
            else
            {
                parseNotes(json, function(err, notes)
                {
                    if(err) callback(err);
                    else callback(null, notes.find(x => x.id === id));
                });
            }
        });
    }
    
    static getAll(callback)
    {
        readJsonFile(function(err, json)
        {
            if(err) callback(err);
            else
            {
                parseNotes(json, function(err, notes)
                {
                    if(err) callback(err);
                    else callback(null, notes);
                });
            }
        });
    }

    static insert(title, noteText, user, importance, file, callback)
    {
        readJsonFile(function(err, json)
        {
            if(err) callback(err);
            else
            {
                parseNotes(json, function(err, notes)
                {
                    if(err) callback(err);
                    else
                    {
                        let today = new Date();
                        today = today.getFullYear() + "-" + today.getMonth() + "-" + today.getDate() + "T" +
                            today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds() + "Z";

                        let fileName = "";
                        let filePath = "";
                        if(file)
                        {
                            fileName = today.toString() + '_' + user.toString() + file.name.substr(file.name.lastIndexOf("."));
                            filePath = './data/fs/' + fileName;
                        }

                        const note = new Note(json.nextId, title, noteText, user, importance, noteText.toString().length, fileName, today);
                        json.nextId++;
                        notes.push(note);
                
                        json.items = JSON.parse(JSON.stringify(notes));

        
                        writeJsonFile( JSON.stringify(json, null, "\t"), err =>
                        {
                            if(err)
                            {
                                callback(err);
                            }
                            else
                            {
                                if(filePath !== "")
                                    fs.writeFile(filePath, file.data, err =>
                                    {
                                        if(err) callback(err);
                                        else callback(null, note);
                                    });
                                else callback(null, note);
                            }
                        });
                    }
                });
            }
        });
    }

    static update(id, title, noteText, user, importance, file, callback)
    {
        readJsonFile(function(err, json)
        {
            if(err) callback(err);
            else
            {
                parseNotes(json, function(err, notes)
                {
                    if(err) callback(err);
                    else
                    {

                        const oldNote = notes.find(x => x.id === id);
                        if(!oldNote)
                        {
                            return oldNote;
                        }
                
                        const index = notes.indexOf(oldNote);
                        if (index > -1) {
                            notes.splice(index, 1);
                        }

                        let today = new Date();
                        today = today.getFullYear() + "-" + today.getMonth() + "-" + today.getDate() + "T" +
                            today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds() + "Z";
                
                        const note = new Note(id, title, noteText, user, importance, noteText.toString().length, file, today);
                        notes.push(note);
                        
                        json.items = JSON.parse(JSON.stringify(notes));
                
                        writeJsonFile( JSON.stringify(json, null, "\t"), err =>
                        {
                            if(err) callback(err);
                            else callback(null, note);
                        });
                    }
                });
            }
        });
    }

    static delete(id, callback)
    {
        readJsonFile(function(err, json)
        {
            if(err) callback(err);
            else
            {
                parseNotes(json, function(err, notes)
                {
                    if(err) callback(err);
                    else
                    {
                        id = parseInt(id);
                        const note = notes.find(x => x.id === id);
                        if(!note)
                        {
                            return note;
                        }
                
                        const index = notes.indexOf(note);
                        if (index > -1) {
                            notes.splice(index, 1);
                        }
                
                        json.items = JSON.parse(JSON.stringify(notes));
                
                        fs.unlink("./data/fs/"+ note.file, err =>
                        {
                            if(err) callback(err);
                            else callback(null, note);
                        });
                    }
                });
            }
        });
    }
}


const fs = require('fs');

module.exports = Note;

function writeJsonFile(data, callback)
{
    fs.writeFile('data/notes.json', data, err =>
    {
        if(err) callback(new Error(err));
        else callback(null);
    });
}

function readJsonFile(callback)
{
    fs.readFile('data/notes.json', (err, data) =>
    {
        if(err)
            callback(new Error(err));
        else
        {
            const json = JSON.parse(data.toString());

            callback(null, json);
        }
    });
}

function parseNotes(json, callback)
{
    let notes = [];
    for(let i = 0; i < json.items.length; i++)
    {
        notes.push(new  Note(json.items[i].id,
            json.items[i].title,
            json.items[i].note,
            json.items[i].user,
            json.items[i].importance,
            json.items[i].length,
            json.items[i].file,
            json.items[i].dateOfCreation));
    }

    callback(null, notes);
}
